function rollDice() {
	var die1 = 0;
	var die2 = 0;
	var betAmt = document.getElementById("betAmt").value;
	var startingBet = Number(betAmt);
	var numRolls = 0;
	var maxMoneyRoll = 1; //PossiblyRemove
	var maxBankroll = startingBet;

	if (betAmt <= 0 || isNaN(betAmt)) {
		return alert("You must bet money to play!");
	}

	var bankroll = Number(betAmt);
	while (bankroll > 0) {
		var d1 = Math.floor(Math.random() * 6) + 1;
		var d2 = Math.floor(Math.random() * 6) + 1;
		var diceTotal = d1 + d2;
		console.log(bankroll);
		if (diceTotal == 7) {
			bankroll += 4;
		}
		else {
			bankroll--;
		}
		numRolls++;
		if(bankroll > maxBankroll) {
			maxBankroll = bankroll;
			maxMoneyRoll = numRolls;
		}

		rollHistory(numRolls,diceTotal,bankroll)
	}

	output(betAmt,numRolls,maxBankroll,maxMoneyRoll);
}

function output(startingBet,num,maxBank,maxRoll) {
	document.getElementById("startingBet").innerHTML = Number(startingBet).toFixed(2);
	document.getElementById("numRollsResult").innerHTML = num;
	document.getElementById("largestBankrollResult").innerHTML = maxBank.toFixed(2);
	document.getElementById("maxMoneyRollResult").innerHTML = maxRoll;
	document.getElementById("submit").value = "Play Again?"
}

function rollHistory(rollNumber, rollOutcome, totalMoney) {
	var rollNumberStack = [];
	var rollOutcomeStack = [];
	var totalMoneyStack = [];
	rollNumberStack.push(rollNumber);
	rollOutcomeStack.push(rollOutcome);
	totalMoneyStack.push(totalMoney);
}