function validateForm() {
	var name = document.forms["contactUs"]["uName"].value;
	var email = document.forms["contactUs"]["email"].value;
	var phone = document.forms["contactUs"]["phone"].value;
	var reason = document.getElementById("reason").selectedIndex;
	var moreInfo = document.forms["contactUs"]["additInfo"].value;

	var m = document.forms.contactUs.bestDayM.checked;
	var t = document.forms.contactUs.bestDayT.checked;
	var w = document.forms.contactUs.bestDayW.checked;
	var th = document.forms.contactUs.bestDayTh.checked;
	var f = document.forms.contactUs.bestDayF.checked;


	if (name==null||name=="") {
		alert("Name must be filled out");
		return false;
	}
	else if((email==null||email=="")&&(phone==null||phone=="")) {
		alert("Please give us a way to contact you!");
		return false;
	}

	if (reason==3 && (moreInfo==null||moreInfo=="")) {
		alert("Please give us some additional information on your reason for inquiry!");
		return false;
	}

	if (!(m||t||w||th||f)) {
		alert("Please select a best day to contact you!");
		return false;
	}
}